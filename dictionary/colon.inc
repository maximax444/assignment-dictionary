%define LAST_EL 0
%macro colon 2
	%ifid %2			
		%2: dq LAST_EL
		%define LAST_EL %2
	%else
		%fatal "incorrect id"
	%endif

	%ifstr %1
		db %1, 0
	%else
		%fatal "incorrect string"
	%endif
%endmacro 
