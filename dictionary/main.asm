
%include "words.inc"
%include "lib.inc"

%define BUFF 256
%define NODE 8

extern find_word

global _start







section .rodata
err_found: ; Ошибка - не найден ключ
	db "not found", 0
err_overflow: ; Ошибка - буфер переполнен
	db "buffer overflow", 0

section .bss
buffer: resb BUFF

section .text

_start:
	xor rax, rax ; обнуляем результат
	mov rdi, buffer ; Помещаем в регистры инфу о буфере
	mov rsi, BUFF ; Помещаем в регистры инфу о буфере
	call read_word ; читаем со ввода слово
	cmp rax, 0 ; Проверяем, что введено корректно
	je .over ; Если нет - переходим к ошибке переполнения буфера
	.readed:
		mov rdi, rax ; переносим введенное в rdi
		mov rsi, LAST_EL ; в rsi пишем последний элемент
		push rdx ; Сохраняем rdx (там длина слова read word)
		call find_word ; ищем вхождение
		pop rdx ; Возвращаем rdx
		cmp rax, 0 ; Не нашли элемент?
		jne .key_normal ; Если нашли - переходим
	.key_err:
		mov rdi, err_found ; выкидываем ошибку и завершаем работу
		jmp .end_err
	.over:
		mov rdi, err_overflow ; Если нет - выкидываем ошибку и завершаем работу
	.end_err:
		call print_error
		call print_newline
		call exit
	.key_normal:
		add rax, NODE ; значение
		add rax, rdx ; добавляем длину слова
		inc rax ; нт 
		mov rdi, rax ; записываем в регистр для вывода
	.end_normal:
		call print_string
		call print_newline
		call exit