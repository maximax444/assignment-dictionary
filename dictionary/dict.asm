global find_word
extern string_equals

%define NODE_SIZE 8


section .text

; Получаем в rdi строку, в rsi ссылку на словарь, возвращаем в rax ссылку на ключ или 0 в случае ненахождения
find_word:
    cmp rsi, 0 ; Проверка на пустоту словаря
    je .notFound ; Окончае работы функции 0
    cmp rdi, 0 ; Проверка на пустоту строки
    je .notFound ; Окончае работы функции 0

    add rsi, NODE_SIZE ; Ищем по строке, а не по ключу, переходим к нужному адресу
    push rdi ; Сохраняем регистры
    push rsi ; Сохраняем регистры
    call string_equals ; Проверка строк, в rdi и rsi указатели на сравниваемые строки (в rax получаем 0 или 1)
    pop rsi ; Возвращаем регистры
    pop rdi ; Возвращаем регистры
    sub rsi, NODE_SIZE ; возвращаем ссылку на ключ

    cmp rax, 1 ; Одинаковы? 
    je  .found ; нашли, переходим к концу
    mov rsi, [rsi] ; следующий элемент
    jmp find_word ; проверим следующий элемент

    .found:
        
        mov rax, rsi ; возвращаем в rax ссылку на ключ
        ret ; окончание работы функции
    .notFound:
        xor rax, rax ; обнуление rax
        ret ; окончание работы функции
